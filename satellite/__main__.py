import sys
from .application import SatelliteApp


def main():
    app = SatelliteApp()
    app.run()
    app.quit_function()
    sys.exit(0)


if __name__ == '__main__':
    main()
